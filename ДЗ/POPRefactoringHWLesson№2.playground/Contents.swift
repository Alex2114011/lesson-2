import UIKit

// Перед тем как делать, можно посмотреть вот это. Своеобразное введение в protocol oriented programming
// https://www.raywenderlich.com/6742901-protocol-oriented-programming-tutorial-in-swift-5-1-getting-started
// Но если любите по харду, то можете начать сами, а потом посмотреть.))


// Если вам будет лень придумывать героев, вот ссылка на их источник
//https://liquipedia.net/warcraft/Races

// Задание #1 (обязательно)
// Перепишите из OOP -> POP
// Попробуйте использовать Traint/Mexin
// По желанию можно выполнить необязательные задания

// Совет:
// Предлагаю использовать ульту(способность) героев в виде Enum при проектировании
//enum AttakStyle: String {
//    case melee = "melee attak!"
//    case longRange = "long range attak!"
//}

// Задание №2 (необязательно)
// Добавьте Гильдию. Мы можем это использовать, чтобы ненужно было создавать отдельную структуру под Orc
//enum GuildType {
//    case aliance
//    case orc
//}

// Задание №3 (необязательно)
// Добавьте свой-во currentPosition: CGPoint. Теперь Героев можно передвигать и при этом у них будет меняться currentPosition. Дороботайте для этого функцию move()

// Задание №4 (необязательно)
// Добавьте возможность героям летать. func fly(to point: CGPoint)


// Далее Задание №5 (необязательно)
// Подходите к заданию творчески, используйте свой функционал, придумывайте его не надо стесняться)))
// 5.1 Создайте свои структуры со своими героями и положите их в массив.
// 5.2 Отсортируйте массивы с героями отделяя Орду от Альянса.
// 5.3 Отсортируйте по мощности атаки attackPower
// 5.4 Найдите самого сильного в массиве (attackPower) или того у кого больше всего mana

// Задание №6 (необязательно)
// Придумайте как можно использовать Dictionary для хранения героев
// Используйте пример из презы_)

// MARK: - ДЗ(основное)
enum AttackStyle: String {
    case stormBolt
    case blizzard
    case holyLight
    case flameStrike


    var attackName: String {
        switch self {
        case .stormBolt: return "Storm Bolt"
        case .blizzard: return "Blizzard"
        case .holyLight: return "Holy Light"
        case .flameStrike: return "FlameStrike"
        }
    }
}

protocol Stats {
    var armor: Float? { get }
    var attackPower: Float? { get }
    var strength: Float? { get }
    var agility: Float? { get }
    var intelligence: Float? { get }
    var hitPoints: Float? { get }
    var mana: Float? { get }

    mutating func updateStatistics(_ statistics: Statistics)
}

struct Statistics: Stats {
    private(set) var armor: Float?
    private(set) var attackPower: Float?
    private(set) var strength: Float?
    private(set) var agility: Float?
    private(set) var intelligence: Float?
    private(set) var hitPoints: Float?
    private(set) var mana: Float?

    mutating func updateStatistics(_ statistics: Statistics) {
        self.armor = statistics.armor
        self.attackPower = statistics.attackPower
        self.strength = statistics.strength
        self.agility = statistics.agility
        self.intelligence = statistics.intelligence
        self.hitPoints = statistics.hitPoints
        self.mana = statistics.mana
    }
}

protocol AlienceHeroes {
    func ultimate()
    func info()
}

extension AlienceHeroes {
    func info() {
        print("\(String(describing: self))")
    }
}

protocol Hero {
    var statistic: Statistics { get set }
    func attack()
}

protocol Moveable {
    func move()
    func stop()
}

extension Moveable {
    func move() {
        print("go to")
    }
    func stop() {
        print("stop")
    }
}

class MountainKing: Hero, AlienceHeroes, Moveable {

    var statistic: Statistics

    init(statistic: Statistics) {
        self.statistic = statistic
    }

    func attack() {
        AttackStyle.stormBolt.attackName
    }

    func ultimate() {
        print("Avatar")
    }
}

class Archmage: Hero, AlienceHeroes, Moveable {

    var statistic: Statistics

    init(statistic: Statistics) {
        self.statistic = statistic
    }

    func attack() {
        AttackStyle.blizzard.attackName
    }
    func ultimate() {
        print("Mass Teleport")
    }
}

class Paladin: Hero, AlienceHeroes, Moveable {

    var statistic: Statistics

    
    init(statistic: Statistics) {
        self.statistic = statistic
    }

    func attack() {
        AttackStyle.holyLight.attackName
    }

    func ultimate() {
        print("Resurrection")
    }
}

class BloodMage: Hero, AlienceHeroes, Moveable {
    var statistic: Statistics

    init(statistic: Statistics) {
        self.statistic = statistic
    }

    func attack() {
        AttackStyle.flameStrike.attackName
    }

    func ultimate() {
        print("Phoenix")
    }
}


let mountainKingStats = Statistics(armor: 2, attackPower: 26, strength: 24, agility: 11, intelligence: 15, hitPoints: 700, mana: 0.76)
let mountainKing = MountainKing(statistic: mountainKingStats)
let newMountainKingStats = Statistics(armor: 20, attackPower: 13, strength: 10, agility: 00, intelligence: 15, hitPoints: 700, mana: 2)
mountainKing.attack()
mountainKing.ultimate()
mountainKing.move()
mountainKing.stop()
mountainKing.info()
mountainKing.statistic.mana
mountainKing.statistic.updateStatistics(newMountainKingStats)
mountainKing.statistic.mana



//MARK: - example
/*
class Alliance {
    typealias Statistics = (armor: Float, attackPower: Float, strength: Float, agility: Float, intelligence: Float, hitPoints: Float, mana: Float)

    private(set) var armor: Float?
    private(set) var attackPower: Float?
    private(set) var strength: Float?
    private(set) var agility: Float?
    private(set) var intelligence: Float?
    private(set) var hitPoints: Float?
    private(set) var mana: Float?

    init(statistics: Statistics) {
        self.armor = statistics.armor
        self.attackPower = statistics.attackPower
        self.strength = statistics.strength
        self.agility = statistics.agility
        self.intelligence = statistics.intelligence
        self.hitPoints = statistics.hitPoints
        self.mana = statistics.mana
    }

    func attack() {
        print("attack")
    }

    func move() {
        print("move")
    }

    func stop() {
        print("stop")
    }

    func updateStatistics(_ statistics: Statistics) {
        self.armor = statistics.armor
        self.attackPower = statistics.attackPower
        self.strength = statistics.strength
        self.agility = statistics.agility
        self.intelligence = statistics.intelligence
        self.hitPoints = statistics.hitPoints
        self.mana = statistics.mana
    }

    // Alliance functions
    func ultimate() {
    }

    func info() {
        print("\(String(describing: self))")
    }
}

class Archmage: Alliance {
    override func attack() {
        print("long-range attack")
    }

    override func ultimate() {
        super.ultimate()
        print("Avatar")
    }
}

class MountainKing: Alliance {
    override func attack() {
        print("melee attacks")
    }

    override func ultimate() {
        super.ultimate()
        print("Mass Teleport")
    }

    override func info() {
        super.info()
    }
}
*/
