//
//  AppDelegate.swift
//  CourseDelegateProtocols
//
//  Created by Курганов Сергей Владимирович on 23.01.2022.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        
        window?.rootViewController = ViewController()
        window?.makeKeyAndVisible()
        return true
    }
}

